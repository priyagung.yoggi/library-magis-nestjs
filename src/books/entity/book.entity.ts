import { Column, PrimaryGeneratedColumn } from "typeorm";

export class BookEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    isbn: string;

    @Column()
    price: number;

    @Column()
    edition: string;
    

}