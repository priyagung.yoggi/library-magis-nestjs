import {Column,CreateDateColumn, Entity, PrimaryGeneratedColumn} from 'typeorm'
export enum roles {
    staff = 'Staff',
    reader =  'Reader'
}
@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    firstname: string;

    @Column()
    lastname: string;

    @Column()
    fullname: string;

    @Column({
        unique: true
    })
    email: string;

    @Column()
    address_1: string;

    @Column()
    address_2: string;

    @Column()
    phone: string;

    @Column()
    avatar: string;

    @Column()
    password: string;

    @CreateDateColumn()
    created_at: Date;

    @Column({
        type: 'enum',
        enum: roles,
        default: roles.staff
    })
    role: roles;



}
